# Git Pull & Git Merge

## References

- <https://git-scm.com/book/fr/v2/Les-branches-avec-Git-Rebaser-Rebasing>

- <https://sdq.kastel.kit.edu/wiki/Git_pull_--rebase_vs._--merge>

- <https://www.atlassian.com/git/tutorials/syncing/git-pull>

## Rebasing

### git pull

Fetch the specified remote’s copy of the current branch and immediately merge it into the local copy.

```sh
git pull <remote>

It's same as :
1. git fetch ＜remote＞
2. git merge origin/＜current-branch＞
```

### git pull --no-commit

Similar to the **pull command**, it fetches the remote content **but does not create a new merge commit**.

```sh
git pull --no-commit <remote>
```

### git pull --rebase

Same as the previous **pull command**, Instead of using _git merge_ to integrate the remote branch with the local one, it use _git rebase_.

```sh
git pull --rebase <remote>
```

If you pull remote changes with the flag `--rebase`, then your local changes are applied on top of the remote changes.


### Best practices

It is always a best practice to **always rebases your local commits** when you pull before pushing them.

To avoid typing `--rebase` whenever you pull you can config git to use it as default:

```sh
git config --global pull.rebase true
```
