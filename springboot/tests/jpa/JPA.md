# Spring boot JPA Tests

## References

- <https://reflectoring.io/spring-boot-data-jpa-test/>

- <https://www.bezkoder.com/spring-boot-unit-test-jpa-repo-datajpatest/>

## Usage: @DataJpaTest

- it set up a Spring application context
- It is used to test JPA repositories in Spring Boot applications.
- It’s a specialized test annotation that provides a minimal Spring context for testing the persistence layer
- @DataJpaTest is the annotation that Spring supports for a JPA test that focuses only on JPA components.
