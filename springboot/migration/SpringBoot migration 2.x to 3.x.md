#migration

Migration can be done in two steps:

1. Step1: Use of **openRewrite** to perform basics migration of simple classes

2. Step2: Finish the migration "manually"

## What is OpenWrite

#openRewrite

Automatic migration

### Manually migration

Steps:

1. install jdk17

2. update springboot version 

	1. if you use openRewrite, it will be done bu it
	
3. Review existing dependencies

	1. Review specific to spring security dependency
	


# References

- https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-3.0-Migration-Guide