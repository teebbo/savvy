
# Déclaration revenus locatif étrangers au fisc français

## References

- https://www.l-expert-comptable.com/a/53002-declaration-de-biens-l-etranger-au-fisc-francais.html
- https://www.belvilla.fr/louer-sa-maison-de-vacances/info/declarer-bien-immobilier-etranger/
- https://www.avocats-picovschi.com/faut-il-declarer-les-biens-immobiliers-detenus-a-l-etranger_article_1768.html
- https://www.gererseul.com/actualites-immobilieres/faut-il-declarer-les-biens-immobiliers-detenus-a-letranger/

## Résumé
### Doit-on déclarer un bien immobilier à l’étranger quand on vit en France ?

Si votre **résidence secondaire à l’étranger** ***ne génère aucun revenu***, **vous n’êtes pas tenu de déclarer son existence à l’administration fiscale**.

Les **biens immobiliers qui génèrent un revenu**, tels que les immeubles locatifs ou les maisons de vacances que vous louez, **devront être déclarés à l’administration fiscale française quelle que soit leur valeur**.

### Comment déclarer les revenus locatifs d’une propriété à l’étranger ?

Si vous louez une propriété à l’étranger, vous devrez déclarer les revenus locatifs dans votre déclaration fiscale annuelle en France. Toutefois, deux options différentes sont possibles pour déclarer ces revenus (quel que soit le type de propriété) :

#### Convention fiscale entre la France et le pays étranger

Afin d’éviter la double imposition de ses citoyens, la France a mis en place des conventions fiscales internationales avec certains pays. Ces conventions fiscales établissent la priorité d’imposition d’un pays par rapport à l’autre.

Cela signifie qu’en vérifiant la convention fiscale signée entre la France et le pays où se trouve votre résidence secondaire, vous comprendrez où vos revenus locatifs seront imposés (que ce soit en France ou dans le pays étranger), et éviterez la double imposition de ces revenus.

**!!!**
Sachez toutefois que cette ***convention fiscale ne vous empêche pas de déclarer ces revenus locatifs à l’administration fiscale française***. ***Vous devez les déclarer***, mais ils ne seront pas imposés deux fois. 
**!!!**

**!!!**
De plus, même si ces revenus ne seront pas imposés deux fois, ils seront tout de même **utilisés pour calculer votre revenu total**, et déterminer le **taux d’imposition** dans lequel vous vous situerez (C’est ce qu’on appelle le « taux effectif »).
**!!!**

#### Absence de convention fiscale entre la France et le pays étranger

Dans le cas où il n’existe pas de convention fiscale entre la France et le pays où se situe votre résidence secondaire, alors vous devrez déclarer ces revenus locatifs dans les deux pays.

Vous devrez **payer des impôts sur ces revenus locatifs dans le pays étranger, ainsi qu’en France par le biais de votre déclaration annuelle de revenus française**. 

Toutefois, vous pouvez **déduire les impôts que vous avez déjà payés sur ces revenus locatifs dans le pays étranger**, dans votre déclaration de revenus française.

