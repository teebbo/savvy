# CONVENTION FRANCE & AFRIQUE DU SUD EN VUE D'EVITER LES DOUBLES IMPOSITIONS ET DE PREVENIR L'EVASION ET LA FRAUDE FISCALES EN MATIERE D'IMPOTS SUR LE REVENU ET SUR LA FORTUNE

# References

- south africa => https://www.sars.gov.za/individuals/tax-during-all-life-stages-and-events/tax-and-non-residents/

Double Taxation Agreements (DTAs)

**_Rental income_**  

The source of rental income is generally regarded to be where the property is used on a day-to-day basis. Rental income which arises in South Africa, received by or accrued to a non-resident will be subject to normal tax in South Africa. Expenses such as rates and taxes, bond interest, insurance, repairs may be claimed as deductions against such rental income, subject to certain conditions.

- service public france pour la convention
- https://www.expatica.com/za/finance/taxes/income-tax-south-africa-105863/#2025-tax-year

### South African income tax for foreigners

Foreign residents pay the same income tax as South African citizens and permanent residents. However, taxes for non-resident taxpayers only apply to income from South Africa and not worldwide. SARS has implemented this policy to ensure that individuals moving from one country to another are not taxed twice on income earned in their home country.

## Article 1: Personnes visées

 => résident d'un état contractant ou des 2 états contractants

## Article 2: impôts visés

paragraphe 2.3

- En France (impôt francais):

  - Impôt sur le revenu,
  - Impôt sociéte
  - impôt sur la forturne pour la France

- En Afrique du Sud (impôt sud-africain):

  - impot normal
  - impot des actionnaires

## Article 3: Définitions générales

Au sens de la présente Convention:

- **Etat contractant** = France (Toutes les régions de France)
- **Autre Etat contractant** = Afrique du Sud (Tout le térritoire Sud Africain)
- Personne = Personne physique & personne morale

## Article 4: Résident

- En France: toute personne assujettie à l'impôt sur le revenu en raisn de son domicile, de sa résidence (foyer d'habitation)
- En Afrique du Sud: Toute personne qui est ordinairement résidente d'Afrique du Sud ou toute personne morale dont le siège de direction effective est situé en Afrique du Sud.

## Article 5: Etablissement stable

=> Installation fixe d'affaires par l'intermédiaire de laquelle une entreprise exerce tout ou partie de son activité

## Article 6: Revenus immobiliers

1. Les revenus qu'un résident d'un **Etat contractant** tire de **biens immobiliers** (y compris les revenus des exploitations agricoles ou forestières) *situés* dans **l'autre Etat contractant** ***sont imposables dans cet autre Etat.***

2. L'expression "biens immobiliers" a le sens que lui attribue le droit de l'Etat contractant où les biens considérés sont situés.

3. Les dispositions du paragraphe 1 s'appliquent aux ***revenus provenant de l'exploitation directe, de la location*** ou de l'affermage, ainsi que de toute autre forme d'exploitation de
biens immobiliers.

## Article 7: Bénéfices des entreprises

1. Les bénéfices d'une entreprise d'un Etat contractant ne sont imposables que dans cet
Etat, à moins que l'entreprise n'exerce son activité dans l'autre Etat contractant par
l'intermédiaire d'un établissement stable qui y est situé.

Si l'entreprise exerce son activité d'une telle façon, les bénéfices de l'entreprise sont imposables dans l'autre Etat mais uniquement dans la mesure où ils sont imputables à cet établissement stable.

## Article 8: Navigation maritime & aérienne

## Article 9: Entreprises associées

## Article 10: Dividendes

1. Les dividendes payés par une société qui est un résident d'un Etat contractant à un
résident de l'autre Etat contractant sont imposables dans cet autre Etat.

4. Le terme " dividende " employé dans le présent article désigne les revenus provenant
d'actions, actions ou bons de jouissance, parts de mine, parts de fondateur ou autres parts
bénéficiaires à l'exception des créances, ainsi que les revenus soumis au régime des
distributions par la législation fiscale de l'Etat contractant dont la société distributrice est un
résident

## Article 11: Intérêts

1. Les intérêts provenant d'un Etat contractant et payés à un résident de l'autre Etat
contractant ne sont imposables que dans cet autre Etat, si ce résident en est le bénéficiaire
effectif et si ces intérêts sont soumis à l'impôt dans cet autre Etat

## Article 12: Redevances

1. Les redevances provenant d'un Etat contractant et payées à un résident de l'autre Etat
contractant ne sont imposables que dans cet autre Etat, si ce résident en est le bénéficiaire
effectif et si ces redevances sont soumises à l'impôt dans cet autre Etat.

## Article 13: Gains en capital

1. a) Les gains qu'un résident d'un Etat contractant tire de l'aliénation de biens immobiliers visés à l'article 6 et situés dans l'autre Etat contractant sont imposables dans cet autre Etat.

 b) Les gains provenant de l'aliénation d'actions, parts ou autres droits dans une société ou personne morale...

## Article 14: Professions indépendantes

1. Les revenus qu'un résident d'un Etat contractant tire d'une profession libérale ou d'autres activités de caractère indépendant ne sont imposables que dans cet Etat, à moins que ce résident ne dispose de façon habituelle dans l'autre Etat contractant d'une base fixe pour l'exercice de ses activités

## Article 15: Professions dépendants

1. Sous réserve des dispositions des articles 16, 18 et 19, les salaires, traitements et autres rémunérations similaires qu'un résident d'un Etat contractant reçoit au titre d'un emploi salarié ne sont imposables que dans cet Etat, à moins que l'emploi ne soit exercé dans l'autre Etat contractant.

Si l'emploi y est exercé, les rémunérations reçues à ce titre sont imposables dans cet autre Etat.

## Article 16: Jetons de présence

"membre du conseil d'administration"

## Article 17: Artistes et sportifs

## Article 18: Pensions et rentes

## Article 19: Rémunérations publiques

## Article 20: Etudiants

## Article 21: Autres revenus

1. Les éléments du revenu d'un résident d'un Etat contractant, d'où qu'ils proviennent, qui ne sont pas traités dans les articles précédents de la présente Convention ne sont imposables que dans cet Etat, à condition qu'ils soient soumis à l'impôt dans cet Etat.

## Article 22: Fortune

1.

=> a. La fortune constituée par des biens immobiliers visés à l'article 6, que possède un résident d'un Etat contractant et qui sont situés dans l'autre Etat contractant, est imposable dans cet autre Etat

=> b. Les actions, parts ou autres droits dans une société ou personne morale

## Article 23: Elimination des doubles impositions

1. En ce qui concerne la France, les doubles impositions sont évitées de la manière
suivante :

a) Les revenus qui proviennent d'Afrique du Sud, et qui sont imposables ou ne sont
imposables que dans cet Etat conformément aux dispositions de la présente Convention,
sont pris en compte pour le calcul de l'impôt français lorsque leur bénéficiaire est un résident de France et qu'ils ne sont pas exemptés de l'impôt sur les sociétés en application de la législation interne française. Dans ce cas, l'impôt sud-africain n'est pas déductible de ces revenus, mais le bénéficiaire a droit à un crédit d'impôt imputable sur l'impôt français. Ce crédit d'impôt est égal :

i) pour les revenus non mentionnés au ii, au montant de l'impôt français
correspondant à ces revenus ;

ii) pour les revenus visés aux paragraphe 2 de l'article 10, paragraphe 1 de l'article 13, paragraphe 3 de l'article 15, paragraphe 1 de l'article 16 et paragraphes 1 et 2 de l'article 17, au montant de l'impôt payé en Afrique du Sud conformément aux dispositions de ces articles ; toutefois, ce crédit d'impôt ne peut excéder le montant de l'impôt français correspondant à ces revenus. L'expression "**montant de l'impôt payé en Afrique du Sud**" désigne le montant de l'impôt sud-africain effectivement supporté à titre définitif à raison des revenus considérés, conformément aux dispositions de la Convention, par le résident de France bénéficiaire de ces revenus;

b)

i) si la législation interne française autorise les sociétés qui sont des résidents de France à déterminer leurs bénéfices imposables en fonction d'une consolidation englobant notamment les résultats de filiales qui sont des résidents d'Afrique du Sud ou d'établissements stables situés en Afrique du Sud, les dispositions de la Convention ne s'opposent pas à l'application de cette législation ;

ii) si, conformément à sa législation interne, la France détermine les bénéfices
imposables de résidents de France en déduisant les déficits de filiales qui sont des
résidents d'Afrique du Sud ou d'établissements stables situés en Afrique du Sud, et
en intégrant les bénéfices de ces filiales ou de ces établissements stables à
concurrence du montant des déficits déduits, les dispositions de la Convention ne
s'opposent pas à l'application de cette législation ;

iii) rien dans la Convention n'empêche la France d'appliquer les dispositions de
l'article 209 B de son code général des impôts ou d'autres dispositions analogues
qui amenderaient ou remplaceraient celles de cet article ;

c) Un résident de France qui possède de la fortune imposable en Afrique du Sud
conformément aux dispositions des paragraphes 1 ou 2 de l'article 22 est également
imposable en France à raison de cette fortune. L'impôt français est calculé sous déduction d'un crédit d'impôt égal au montant de l'impôt payé en Afrique du Sud sur cette fortune. Ce crédit ne peut toutefois excéder le montant de l'impôt français correspondant à cette fortune.

2. En ce qui concerne l'Afrique du Sud, les doubles impositions sont évitées de la manière suivante : l'impôt payé par les résidents d'Afrique du Sud au titre des revenus ou de la fortune imposables en France conformément aux dispositions de la Convention est déduit de l'impôt sud-africain exigible. Toutefois, cette déduction ne peut excéder la part de l'impôt sur le revenu ou sur la fortune, calculé avant déduction, correspondant aux revenus ou à la fortune imposables en France.

3. a) En qui concerne les dispositions des a et c du paragraphe 1 :

i) il est entendu que l'expression " montant de l'impôt français correspondant à ces
revenus " désigne :

aa) lorsque l'impôt dû à raison de ces revenus est calculé par application d'un
taux proportionnel, le produit du montant des revenus nets considérés par le taux
qui leur est effectivement appliqué ;

bb) lorsque l'impôt dû à raison de ces revenus est calculé par application d'un
barème progressif, le produit du montant des revenus nets considérés par le taux
résultant du rapport entre l'impôt effectivement dû à raison du revenu net global
imposable selon la législation française et le montant de ce revenu net global ;

ii) la même interprétation s'applique par analogie à l'expression " montant de l'impôt français correspondant à cette fortune " ;

b) En ce qui concerne l'application des dispositions du paragraphe 2, il est entendu que le montant de l'impôt correspondant aux revenus ou aux éléments de fortune soumis à l'impôt en France est égal :

i) lorsque l'impôt dû à raison de ces revenus ou de ces éléments de fortune est
calculé par application d'un taux proportionnel, au produit du montant des revenus
nets considérés ou de la valeur nette des éléments de fortune considérés par le
taux qui leur est effectivement appliqué ;

ii) lorsque l'impôt dû à raison de ces revenus ou de ces éléments de fortune est
calculé par application d'un barème progressif, à un montant qui est aux revenus
nets considérés ou à la valeur nette des éléments de fortune considérés ce que le
total de l'impôt effectivement dû est au revenu net global ou à la fortune nette
globale imposable selon la législation sud-africaine

## Article 24: Non-Discrimination

1. Les personnes physiques possédant la nationalité d'un Etat contractant ne sont soumises dans l'autre Etat contractant à aucune imposition ou obligation y relative, qui est autre ou plus lourde que celles auxquelles sont ou pourront être assujetties les personnes physiques possédant la nationalité de cet autre Etat qui se trouvent dans la même situation notamment au regard de la résidence

## Article 25: Procédure amiable

1. Lorsqu'une personne estime que les mesures prises par un Etat contractant ou par les deux Etats contractants entraînent ou entraîneront pour elle une imposition non conforme aux dispositions de la présente Convention, elle peut, indépendamment des recours prévus par le droit interne de ces Etats, soumettre son cas à l'autorité compétente de l'Etat contractant dont elle est un résident. Le cas doit être soumis dans les **trois ans** qui suivent la première notification de la mesure qui entraîne une imposition non conforme aux dispositions de la Convention.

## Article 26: Echange de renseignements

1. Les autorités compétentes des Etats contractants échangent les renseignements
nécessaires pour appliquer les dispositions de la présente Convention, ou celles de la législation interne des Etats contractants relative aux impôts visés par la Convention, dans la mesure où l'imposition qu'elle prévoit n'est pas contraire à la Convention.

## Article 27: Agents diplomatiques et fonctionnaires consulaires

1. Les dispositions de la présente Convention ne portent pas atteinte aux privilèges fiscaux dont bénéficient les membres des missions diplomatiques et leurs domestiques privés, les membres des postes consulaires ou les membres des délégations permanentes auprès d'organisations internationales en vertu soit des règles générales du droit international, soit des dispositions d'accords particuliers.

## Article 28: Dispositions particulières

1. Rien dans la présente Convention n'empêche la France d'appliquer les dispositions de l'article 21 de son code général des impôts ou d'autres dispositions analogues de droit français qui amenderaient ou remplaceraient celles de cet article

2. En ce qui concerne les articles 10 et 11, un fonds ou société d'investissement, [...]

## Article 29: Modalités d'application

1. Les autorités compétentes des Etats contractants peuvent régler d'un commun accord les modalités d'application de la présente Convention.

2. Pour obtenir dans un Etat contractant les avantages prévus par la Convention, les
résidents de l'autre Etat contractant doivent, si les autorités compétentes en disposent ainsi d'un commun accord, présenter un formulaire d'attestation de résidence indiquant en particulier la nature ainsi que le montant ou la valeur des revenus ou de la fortune concernés, et comportant la certification des services fiscaux de cet autre Etat.

## Article 30: Champ d'application territorial

1. La présente Convention peut être étendue, telle quelle ou avec les modifications
nécessaires, aux territoires d'outre-mer et autres collectivités territoriales de la République française, qui perçoivent des impôts de caractère analogue à ceux auxquels s'applique la Convention

## Article 31: Entrée en vigueur

1. Chacun des Etats contractants notifiera à l'autre l'accomplissement des procédures
requises par sa législation pour la mise en vigueur de la présente Convention.

Celle-ci entrera en vigueur le premier jour du second mois suivant le jour de réception de la dernière de ces notifications.

2. Les dispositions de la Convention s'appliqueront :

a) En ce qui concerne les impôts perçus par voie de retenue à la source, aux sommes
imposables après l'année civile au cours de laquelle la Convention est entrée en vigueur ;

## Article 32: Dénonciation

1. La présente Convention demeurera en vigueur sans limitation de durée. Toutefois, après une période de cinq années civiles suivant la date d'entrée en vigueur de la Convention, chacun des Etats contractants pourra la dénoncer moyennant un préavis notifié par la voie diplomatique au moins six mois avant la fin de chaque année civile.
