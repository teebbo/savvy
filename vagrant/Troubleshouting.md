
# WSL Troubleshouting

### Sync folders

`vagrant up` from wsl2
There are errors in the configuration of this machine. Please fix
the following errors and try again:

vm:
* The host path of the shared folder is not supported from WSL. Host
path of the shared folder must be located on a file system with
DrvFs type. Host path: .

- https://emerle.dev/2022/07/18/wsl-and-windows-filesystem/
- https://thedatabaseme.de/2022/02/20/vagrant-up-running-vagrant-under-wsl2/
- https://superuser.com/questions/1658380/how-can-i-sync-folders-stored-within-wsl2-to-virtualbox-guest
- https://github.com/hashicorp/vagrant/issues/10576

### Connection refused

https://github.com/hashicorp/vagrant/issues/12081

The solution seems simple. By installing the Vagrant plugin `virtualbox_WSL2` with the following command:

```bash
vagrant plugin install virtualbox_WSL2
```
