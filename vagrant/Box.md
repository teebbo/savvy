
Every vagrant development environment requires a #box
## Vagrant Box

Box is a package format for vagrant environment.

You specify a box environment and operating configurations in your #Vagrantfile.

**Note**: Boxes require a provider, a virtualization product, to operate. Before you can use a box, ensure that you have properly installed a supported [provider](https://developer.hashicorp.com/vagrant/docs/providers).

Boxes are provider-specific. A box for virtualBox is not compatible with the VMware Fusion provider, or any other provider. A box must be installed for each provider.

So you can have both a VirtualBox and VMware Fusion "bionic64" box

#### Installation

```shell-session
vagrant box add box-name
```

Vagrant now automatically detects what provider a box is for. For example, for "bionic64",
```shell-session
vagrant box list
```

```shell-session
bionic64 (virtualbox)
bionic64 (vmware_fusion)
```

## Vagrant Provider

Main providers supported by vagrant:
- VirtualBox
- Hyper-V
- Docker
- VMware

Vagrant has the ability to manage other types of machines as well. This is done by using other _providers_ with Vagrant.

### Vagrant provider usage

Once a provider is installed, you can use it by calling `vagrant up` with the  `--provider` flag

```shell-session
vagrant up --provider=vmware_fusion
```

#### Default provider

Vagrant attempts to find the default provider in the following order:

1. The `--provider` flag on a `vagrant up` is chosen above all else, if it is present.

2. If the `VAGRANT_DEFAULT_PROVIDER` environmental variable is set, it takes next priority and will be the provider chosen.

3. Vagrant will go through all of the `config.vm.provider` calls in the Vagrantfile and try each in order. It will choose the first provider that is usable. For example, if you configure Hyper-V, it will never be chosen on Mac this way. It must be both configured and usable.

4. Vagrant will go through all installed provider plugins (including the ones that come with Vagrant), and find the first plugin that reports it is usable.  There is a priority system here: systems that are known better have a higher priority than systems that are worse. For example, if you have the VMware provider installed, it will always take priority over VirtualBox.

5. If Vagrant still has not found any usable providers, it will error.

### Vagrant provider configuration

This provider-specific configuration is done within the Vagrantfile in a way that is portable, easy to use, and easy to understand.

#### Portability

An important fact is that even if you configure other providers within a Vagrantfile, the Vagrantfile remains portable even to individuals who do not necessarily have that provider installed.

For example, if you configure VMware Fusion and send it to an individual who does not have the VMware Fusion provider, Vagrant will silently ignore that part of the configuration.

#### Provider Configuration

Configuring a specific provider looks like this:

> [!NOTE]
> Vagrant.configure("2") do |config|
>   ...
>
>   config.vm.provider "virtualbox" do |vb|
>     vb.customize ["modifyvm", :id, "--cpuexecutioncap", "50"]
>   end
> end
>

Multiple `config.vm.provider` blocks can exist to configure multiple providers.

The configuration format should look very similar to how provisioners are configured. The `config.vm.provider` takes a single parameter: the name of the provider being configured. Then, an inner block with custom configuration options is exposed that can be used to configure that provider.

This inner configuration differs among providers, so please read the documentation for your provider of choice to see available configuration options.

#### Overriding Configuration

Providers can also override non-provider specific configuration, such as `config.vm.box` and any other Vagrant configuration. This is done by specifying a second argument to `config.vm.provider`. This argument is just like the normal `config`, so set any settings you want, and they will be overridden only for that provider.

> [!NOTE]
> Vagrant.configure("2") do |config|
>   config.vm.box = "bionic64"
>
>   config.vm.provider "vmware_fusion" do |v, override|
>     override.vm.box = "bionic64_fusion"
>   end
> end
>

In the above case, Vagrant will use the "bionic64" box by default, but will use "bionic64_fusion" if the VMware Fusion provider is used.
# References
- https://developer.hashicorp.com/vagrant/docs/boxes
- https://developer.hashicorp.com/vagrant/docs/providers