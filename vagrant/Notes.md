
#vagrant #virtualisation #provisioning
#hypervisor #hyperviseur

[[Virtualisation]]

Procédé permettant d'émuler des VMs sur une seule machine hôte,  par l’intermédiaire d'hyperviseur (de niveau 2).

Exemple d'hyperviseurs:
- VirtualBox
- VMWare
- Proxmox
- Hyper-V

Vagrant

- Outil permettant la création et la gestion/configurations des VM de manière automatiques
- Offre des environnements de développement cohérent, reproductible et portables.


## What is Vagrant?

Vagrant is the command line utility for managing the lifecycle of virtual machines. Isolate dependencies and their configuration within a single disposable and consistent environment.

Usage
