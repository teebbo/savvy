
### init

**Command: `vagrant init [name [url]]`**

This initializes the current directory to be a Vagrant environment by creating an initial [Vagrantfile](https://developer.hashicorp.com/vagrant/docs/vagrantfile) if one does not already exist.

If a first argument is given, it will prepopulate the `config.vm.box` setting in the created Vagrantfile.

If a second argument is given, it will prepopulate the `config.vm.box_url` setting in the created Vagrantfile.

### Halt

This command shuts down the running machine Vagrant is managing.

**`vagrant halt [name|id]`**
#### Options

- `-f` or `--force` - Do not attempt to gracefully shut down the machine.
# References
- https://developer.hashicorp.com/vagrant/docs/cli/halt#
- https://developer.hashicorp.com/vagrant/docs/cli/init


