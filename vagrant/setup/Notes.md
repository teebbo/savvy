
### Install #vagrant

To install Vagrant, first find the [appropriate package](https://developer.hashicorp.com/vagrant/install) for your system and download it. Vagrant is packaged as an operating-specific package.

It's possible to install Vagrant by using one of the method below:

1. Method 1: Using binary: Download the binary and install it
> [!Using binary]
> 	1. wget https://releases.hashicorp.com/vagrant/2.4.1/vagrant_2.4.1-1_i686.deb
> 	2. sudo apt install vagrant_2.4.1-1_i686.deb

2. Method 2: using package manager

> [!Using package manager]
> 	1. wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
> 	2. echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
> 	3. sudo apt update && sudo apt install vagrant


## Install #vagrant on #wsl

Vagrant _must_ be installed within the Linux distribution used with WSL.
Download the installer package for the Linux distribution from the [releases page](https://releases.hashicorp.com/vagrant/) and install Vagrant.

**NOTE: When Vagrant is installed on the Windows system the version installed within the Linux distribution _must_ match.**

Install vagrant using the binary installation's method

## WSL Vagrant usage

1. Windows access: `VAGRANT_WSL_ENABLE_WINDOWS_ACCESS`

Working within the WSL provides a layer of isolation from the actual Windows system. In most cases Vagrant will need access to the actual Windows system to function correctly.

As most Vagrant providers will need to be installed on Windows directly (not within the WSL) Vagrant will require Windows access. Access to the Windows system is controlled via an environment variable: `VAGRANT_WSL_ENABLE_WINDOWS_ACCESS`. If this environment variable is set, Vagrant will access the Windows system to run executables and enable things like ==**synced folders**==

This will enable Vagrant to access the Windows system outside of the WSL and properly interact with Windows executables. This will automatically modify the `VAGRANT_HOME` environment variable if it is not already defined, setting it to be within the user's home directory on Windows.

So by default Vagrant will not access features available on the Windows system from within the WSL. This means the VirtualBox and Hyper-V providers will not be available. To enable Windows access, which will also enable the VirtualBox and Hyper-V providers, set the environment variable.

2. PATH modification

Vagrant will detect when it is being run within the WSL and adjust how it locates and executes third party executables. For example, when using the VirtualBox provider Vagrant will interact with VirtualBox installed on the Windows system, not within the WSL. It is important to ensure that any required Windows executable is available within your `PATH` to allow Vagrant to access them.

```
export PATH="$PATH:/mnt/c/Program Files/Oracle/VirtualBox"
```

3. Synced folders
4. Other useful WSL related environment variables:
	- [`VAGRANT_WSL_WINDOWS_ACCESS_USER`](https://developer.hashicorp.com/vagrant/docs/other/wsl#vagrant_wsl_windows_access_user) - Override current Windows username
	- [`VAGRANT_WSL_DISABLE_VAGRANT_HOME`](https://developer.hashicorp.com/vagrant/docs/other/wsl#vagrant_wsl_disable_vagrant_home) - Do not modify the `VAGRANT_HOME` variable
	- [`VAGRANT_WSL_WINDOWS_ACCESS_USER_HOME_PATH`](https://developer.hashicorp.com/vagrant/docs/other/wsl#vagrant_wsl_windows_access_user_home_path) - Custom Windows system home path - path to your user home directory from within WSL

# References

- https://developer.hashicorp.com/vagrant/install
- https://developer.hashicorp.com/vagrant/docs/other/wsl