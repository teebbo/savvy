# Maven properties guides

## References

<https://cwiki.apache.org/confluence/display/MAVEN/Maven+Properties+Guide>

## Maven properties

- project.basedir = root folder where the current pom.xml is located

### Build

Below are the **default values** defined in the **Super POM**

- project.build.directory = target/ folder
- project.build.outputDirectory = target/classes folder
- project.build.testOutputDirectory = target/test-classes folder
- project.build.sourceDirectory = src/main/java folder
- project.build.testDirectory = src/test/java folder
- project.build.finalName = ${project.artifactId}-${project.version}
